package azra.wildan.appx07

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DBOpenHelper (context: Context): SQLiteOpenHelper(context,DB_Name, null, DB_Ver) {
    companion object{
        val DB_Name = "aplikasi07"
        val DB_Ver = 1
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val tMahasiswa = "create table mahasiswa(nim text primary key, nama text not null, prodi text not null)"

        db?.execSQL(tMahasiswa)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}